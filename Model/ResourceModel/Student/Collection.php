<?php
/**
 * Collection
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Student\Model\ResourceModel\Student;

use Magestore\Student\Model\Student as Model;
use Magestore\Student\Model\ResourceModel\Student as ResourceModel;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init( Model::class, ResourceModel::class);
    }

}
