<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
//declare(strict_types=1);

namespace Magestore\Student\Model;

use Magento\Framework\Api\SearchResults;

/**
 * Service Data Object with Product search results.
 */
class StudentSearchResults extends SearchResults implements \Magestore\Student\Api\Data\StudentSearchResultInterface
{

}
