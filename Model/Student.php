<?php
/**
 * Student
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Student\Model;

use Magestore\Student\Model\ResourceModel\Student as ResourceModel;

class Student extends \Magento\Framework\Model\AbstractModel
    implements \Magestore\Student\Api\Data\StudentInterface
{
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }

    public function getID()
    {
        return $this->getData(self::ID);
    }

    public function getName()
    {
        return $this->getData(self::NAME);
    }

    public function getClass()
    {
        return $this->getData(self::CLASSNAME);
    }

    public function getUniversity()
    {
        return $this->getData(self::UNIVERSITY);
    }

    public function setID($id)
    {
        return $this->setData(self::ID, $id);
    }

    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    public function setClass($classname)
    {
        return $this->setData(self::CLASSNAME, $classname);

    }

    public function setUniversity($university)
    {
        return $this->setData(self::UNIVERSITY, $university);
    }
}
