<?php
/**
 * StudentRepository
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Student\Model;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class StudentRepository implements \Magestore\Student\Api\StudentRepositoryInterface
{
    private $studentResource;
    private $student;
    /**
     * @var \Magestore\Student\Model\ResourceModel\Student\CollectionFactory
     */
    protected $collectionFactory;

    private $studentSearchResult;

    public function __construct(\Magestore\Student\Model\ResourceModel\Student $studentResource,
                                \Magestore\Student\Model\StudentFactory $student,
                                \Magestore\Student\Model\ResourceModel\Student\CollectionFactory $collectionFactory,
                                \Magestore\Student\Api\Data\StudentSearchResultInterfaceFactory $studentSearchResult)
    {
        $this->studentSearchResult = $studentSearchResult;
        $this->collectionFactory = $collectionFactory;
        $this->studentResource = $studentResource;
        $this->student = $student;
    }

    public function save(
        \Magestore\Student\Api\Data\StudentInterface $student)
    {
        try {
            $this->studentResource->save($student);
        } catch (\Exception $exception) {
            echo $exception;
        }
        return $student;
    }

    public function getById($studentId)
    {
        $student = $this->student->create()->load($studentId);

        if (!$student->getId()) {
            throw new NoSuchEntityException(
                __("The student that was requested doesn't exist. Verify and try again.")
            );
        }
        return $student;
    }

    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->collectionFactory->create();

        $searchResults = $this->studentSearchResult->create();
        $searchResults->setSearchCriteria($searchCriteria);

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $searchResults->setTotalCount($collection->getSize());
        $sortOrdersData = $searchCriteria->getSortOrders();

        if ($sortOrdersData) {
            foreach ($sortOrdersData as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }

        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());
        $searchResults->setItems($collection->getData());
        return $searchResults;
    }

    public function delete(\Magestore\Student\Api\Data\StudentInterface $student)
    {
        try {
            $this->studentResource->delete($student);
            return true;
        } catch (\Exception $exception) {
            echo "failed to delete student";
            return false;
        }
    }


    public function deleteById($studentId)
    {
        try {
            $student = $this->student->create()->load($studentId);
            $this->studentResource->load($student, $studentId);
            $this->delete($student);
            return true;
        } catch (\Exception $exception) {
            echo "failed to delete student";
            return false;
        }
    }
}
