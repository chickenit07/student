<?php


namespace Magestore\Student\Api;


interface StudentRepositoryInterface
{
    /**
     * @param \Magestore\Student\Api\Data\StudentInterface $student
     * @return \Magestore\Student\Api\Data\StudentInterface
     */
    public function save(\Magestore\Student\Api\Data\StudentInterface $student);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magestore\Student\Api\Data\StudentSearchResultInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * @param int $studentId
     * @return \Magestore\Student\Api\Data\StudentInterface
     */
    public function getById($studentId);

    /**
     * @param \Magestore\Student\Api\Data\StudentInterface $student
     * @return bool
     */
    public function delete(\Magestore\Student\Api\Data\StudentInterface $student);

    /**
     * @param int $studentId
     * @return bool
     */
    public function deleteById($studentId);

}
