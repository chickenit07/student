<?php


namespace Magestore\Student\Api\Data;


interface StudentSearchResultInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get attributes list.
     *
     * @return \Magestore\Student\Api\Data\StudentInterface[]
     */
    public function getItems();

    /**
     * Set attributes list.
     *
     * @param \Magestore\Student\Api\Data\StudentInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
