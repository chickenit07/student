<?php


namespace Magestore\Student\Api\Data;


interface StudentInterface
{
    const ID = 'id';

    const NAME = 'name';

    const CLASSNAME ='class';

    const UNIVERSITY = 'university';

    /**
     * @param $id
     * @return $this
     */
    public function setId($id);

    /**
     * @param $name
     * @return $this
     */
    public function setName($name);

    /**
     * @param $class
     * @return $this
     */
    public function setClass($class);

    /**
     * @param $university
     * @return $this
     */
    public function setUniversity($university);

    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getClass();

    /**
     * @return string
     */
    public function getUniversity();
}
