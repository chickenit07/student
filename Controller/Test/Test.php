<?php
/**
 * Test
 *
 * @copyright Copyright © 2020 Staempfli AG. All rights reserved.
 * @author    juan.alonso@staempfli.com
 */

namespace Magestore\Student\Controller\Test;


class Test extends \Magento\Framework\App\Action\Action
{
    private $_modelStudentFactory;
    private $_studentRepository;

    public function __construct(\Magento\Framework\App\Action\Context $context,
                                \Magestore\Student\Model\StudentFactory $modelStudentFactory,
                                \Magestore\Student\Api\StudentRepositoryInterface $studentRepository
    )
    {
        $this->_studentRepository = $studentRepository;
        $this->_modelStudentFactory = $modelStudentFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $student = $this->_modelStudentFactory->create();
//        var_dump($student->load(1)->getData());
        $studentCollection = $student->getCollection();
        var_dump($studentCollection->getData());

//        $studentCollection = $student->getCollection();
//        $listAllIds[] = $studentCollection->getAllIds();

//        try {
//            $data = ['id' => 5,
//                'name' => 'Dat Quang Phamm'
//                        ];
//            $student->setData($data);
//            $student->save();
//            var_dump($student->getData());
//        } catch (\Exception $e) {
//            echo "could not save";
//        }
//        var_dump($student->load(5)->getData());
    }
}
